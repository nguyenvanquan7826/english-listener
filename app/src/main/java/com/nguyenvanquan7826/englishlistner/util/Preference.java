package com.nguyenvanquan7826.englishlistner.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.nguyenvanquan7826.englishlistner.BuildConfig;

public class Preference {
    public static SharedPreferences get(Context context) {
        return context.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
    }

    public static void save(Context context, String key, Object value) {
        SharedPreferences.Editor edit = get(context).edit();
        if (value == null) {
            edit.putString(key, null);
        } else {
            Class<?> fieldType = value.getClass();
            if (fieldType.equals(Integer.class) || fieldType.equals(int.class)) {
                edit.putInt(key, (Integer) value);
            } else if (fieldType.equals(Long.class) || fieldType.equals(long.class)) {
                edit.putLong(key, (Long) value);
            } else if (fieldType.equals(Float.class) || fieldType.equals(float.class)) {
                edit.putFloat(key, (Float) value);
            } else if (fieldType.equals(Boolean.class) || fieldType.equals(boolean.class)) {
                edit.putBoolean(key, (Boolean) value);
            } else if (fieldType.equals(String.class)) {
                edit.putString(key, (String) value);
            }
        }
        edit.apply();
    }
}
