package com.nguyenvanquan7826.englishlistner.util;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nguyenvanquan7826.englishlistner.part.PartSong;

import java.util.ArrayList;
import java.util.List;

public class DbLocalUtil {
    private static final String TAG = DbLocalUtil.class.getSimpleName();

    private static final String KEY_FOLDER_URI = "folder";
    private static final String KEY_LIST_PART = "listPart";
    private static final String KEY_SETTING_SCOND_REPLAY = "replay";

    public static final int[] SECOND_REPLAY_ARR = {5, 10, 30};

    public static void saveSelectFolder(Context context, String uri) {
        Preference.save(context, KEY_FOLDER_URI, uri);
    }

    public static String getSelectFolderUri(Context context) {
        return Preference.get(context).getString(KEY_FOLDER_URI, "");
    }


    private static String createKeyListPart(String fileName) {
        return KEY_LIST_PART + fileName;
    }

    public static void addSavePartSong(Context context, String fileName, PartSong partSong) {
        List<PartSong> partSongList = getPartSongList(context, fileName);
        partSongList.add(partSong);
        Preference.save(context, createKeyListPart(fileName), new Gson().toJson(partSongList));
    }

    public static void removeSavePartSong(Context context, String fileName, long id) {
        List<PartSong> partSongList = getPartSongList(context, fileName);
        for (PartSong partSong : partSongList) {
            if (partSong.getId() == id) {
                partSongList.remove(partSong);
                break;
            }
        }
        Preference.save(context, createKeyListPart(fileName), new Gson().toJson(partSongList));
    }

    public static List<PartSong> getPartSongList(Context context, String fileName) {
        String jsonList = Preference.get(context).getString(createKeyListPart(fileName), "");
        List<PartSong> partSongList = new Gson().fromJson(jsonList, new TypeToken<List<PartSong>>() {
        }.getType());
        if (partSongList == null) partSongList = new ArrayList<>();
        return partSongList;
    }

    public static void savePartSongList(Context context, String fileName, List<PartSong> partSongList) {
        Preference.save(context, createKeyListPart(fileName), new Gson().toJson(partSongList));
    }

    public static void saveSettingIndexSecondReplay(Context context, int second) {
        Preference.save(context, KEY_SETTING_SCOND_REPLAY, second);
    }

    public static int getSettingIndexSecondReplay(Context context) {
        return Preference.get(context).getInt(KEY_SETTING_SCOND_REPLAY, 1);
    }
}
