package com.nguyenvanquan7826.englishlistner.util;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;

public class PermissionUtil {
    public static boolean checkPermissions(Context context, String... permisstions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permisstions != null) {
            for (String permission : permisstions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
}
