package com.nguyenvanquan7826.englishlistner.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public class SupportUtil {
    public static void sendEmail(Context context, String title, String email[], String subject, String sms, String errorSMS) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + email[0]));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, sms);
        context.startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    private static String getAppLink(String appId) {
        return "https://play.google.com/store/apps/details?id=" + appId;
    }

    public static void openPlayStore(Context context, String appId) {
        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getAppLink(appId))));
    }

    public static void shareApp(Context context, String titleChooser, String appId) {
        shareText(context, titleChooser, "", getAppLink(appId));
    }

    public static void shareText(Context context, String titleDialogChooser, String subject, String msg) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        shareIntent.putExtra(Intent.EXTRA_TEXT, msg);
        context.startActivity(Intent.createChooser(shareIntent, titleDialogChooser));
    }
}
