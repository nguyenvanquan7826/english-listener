package com.nguyenvanquan7826.englishlistner.util;

import java.util.Locale;

public class TimeUtil {
    public static String getTextOfTime(int millis) {
        int hours = (millis / (1000 * 60 * 60));
        int minutes = ((millis % (1000 * 60 * 60)) / (1000 * 60));
        int seconds = (((millis % (1000 * 60 * 60)) % (1000 * 60)) / 1000);

        Locale locale = Locale.getDefault();
        String text;
        if (hours > 0) {
            text = String.format(locale, "%02d:%02d:%02d", hours, minutes, seconds);
        } else {
            text = String.format(locale, "%02d:%02d", minutes, seconds);
        }

        return text;
    }
}
