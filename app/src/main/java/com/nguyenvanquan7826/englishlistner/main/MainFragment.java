package com.nguyenvanquan7826.englishlistner.main;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.provider.DocumentFile;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nguyenvanquan7826.englishlistner.R;
import com.nguyenvanquan7826.englishlistner.base.BaseActivity;
import com.nguyenvanquan7826.englishlistner.base.BaseFragment;
import com.nguyenvanquan7826.englishlistner.part.PlayFragment;
import com.nguyenvanquan7826.englishlistner.util.DbLocalUtil;

import java.util.ArrayList;
import java.util.List;

public class MainFragment extends BaseFragment {
    private static final String TAG = MainFragment.class.getSimpleName();
    private static final int FILE_SELECT_CODE = 111;

    private FileAdapter fileAdapter;
    private List<DocumentFile> documentFileList = new ArrayList<>();

    private TextView tvNotify;

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView rv = view.findViewById(R.id.rv);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        fileAdapter = new FileAdapter(getContext(), documentFileList).setOnItemClickListener((position, tag) -> openFile(position));
        rv.setAdapter(fileAdapter);

        tvNotify = view.findViewById(R.id.tvNotify);

        loadFileMp3();
    }

    private void loadFileMp3() {
        String uriString = DbLocalUtil.getSelectFolderUri(getContext());
        Log.e(TAG, "uri:" + uriString);
        if (TextUtils.isEmpty(uriString)) {
            chooserFolder();
        } else {
            displayListFile(Uri.parse(uriString));
        }
    }

    private void chooserFolder() {
        Intent i = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        startActivityForResult(Intent.createChooser(i, getString(R.string.select_folder_contai_mp3)), FILE_SELECT_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    Uri uri = data.getData();
                    displayListFile(uri);
                    DbLocalUtil.saveSelectFolder(getContext(), uri.toString());
                    break;
                }
        }
    }

    private void displayListFile(Uri uri) {
        documentFileList.clear();
        DocumentFile documentFiles[] = DocumentFile.fromTreeUri(getContext(), uri).listFiles();
        for (DocumentFile df : documentFiles) {
            if (df.isFile() && df.getName() != null && df.getName().endsWith(".mp3")) {
                documentFileList.add(df);
            }
        }
        fileAdapter.notifyDataSetChanged();

        tvNotify.setVisibility(documentFileList.isEmpty() ? View.VISIBLE : View.GONE);
    }

    private void openFile(int pos) {
        DocumentFile documentFile = documentFileList.get(pos);
        ((BaseActivity) getActivity()).showFragment(
                PlayFragment.newInstance(documentFile.getUri().toString(), documentFile.getName()), true);
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.list_file));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.main_fragment_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_open_folder:
                chooserFolder();
                break;
        }
        return true;
    }
}
