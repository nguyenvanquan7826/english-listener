package com.nguyenvanquan7826.englishlistner.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.provider.DocumentFile;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nguyenvanquan7826.englishlistner.R;
import com.nguyenvanquan7826.englishlistner.base.BaseRVAdapter;

import java.util.List;

public class FileAdapter extends BaseRVAdapter<FileAdapter.ViewHolder> {
    private Context context;
    private List<DocumentFile> documentFileList;

    public FileAdapter(Context context, List<DocumentFile> documentFileList) {
        this.documentFileList = documentFileList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_file, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        DocumentFile file = documentFileList.get(i);
        viewHolder.tvSongName.setText(file.getName());
    }

    @Override
    public int getItemCount() {
        return documentFileList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvSongName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvSongName = itemView.findViewById(R.id.tvSongName);

            itemView.setOnClickListener(v -> clickItem(getAdapterPosition(), null));
        }
    }
}
