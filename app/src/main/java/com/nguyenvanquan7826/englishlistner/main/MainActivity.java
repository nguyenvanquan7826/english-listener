package com.nguyenvanquan7826.englishlistner.main;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.nguyenvanquan7826.englishlistner.BuildConfig;
import com.nguyenvanquan7826.englishlistner.R;
import com.nguyenvanquan7826.englishlistner.base.BaseActivity;
import com.nguyenvanquan7826.englishlistner.settings.SettingsFragment;
import com.nguyenvanquan7826.englishlistner.support.SupportFragment;
import com.nguyenvanquan7826.englishlistner.util.PermissionUtil;
import com.nguyenvanquan7826.englishlistner.util.SupportUtil;

import java.util.Objects;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    private String[] permissions = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
    };
    private static final int REQUEST_CODE_PERMISSION_READ_STORE = 7826;

    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        bindView();

        if (!PermissionUtil.checkPermissions(this, permissions)) {
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_PERMISSION_READ_STORE);
        } else {
            showMainFragment();
        }
    }

    private void bindView() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        getSupportFragmentManager().addOnBackStackChangedListener(() -> {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                toggle.setDrawerIndicatorEnabled(false);
                Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);// show back button
                toolbar.setNavigationOnClickListener(v -> onBackPressed());
            } else {
                //show hamburger
                toggle.setDrawerIndicatorEnabled(true);
                Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(false);
                toggle.syncState();
                toolbar.setNavigationOnClickListener(v -> drawer.openDrawer(GravityCompat.START));
            }
        });

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_support:
                showFragment(SupportFragment.newInstance(), true);
                break;
            case R.id.nav_share_app:
                SupportUtil.shareApp(this, getString(R.string.choose_one_app_to_share), BuildConfig.APPLICATION_ID);
                break;
            case R.id.nav_settings:
                showFragment(SettingsFragment.newInstance(), true);
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            showMainFragment();
        } else {
            Toast.makeText(this, R.string.we_can_not_read_file, Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private void showMainFragment() {
        showFragment(MainFragment.newInstance(), false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
