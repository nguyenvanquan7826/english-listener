package com.nguyenvanquan7826.englishlistner.support;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nguyenvanquan7826.englishlistner.BuildConfig;
import com.nguyenvanquan7826.englishlistner.R;
import com.nguyenvanquan7826.englishlistner.base.BaseFragment;
import com.nguyenvanquan7826.englishlistner.util.SupportUtil;

public class SupportFragment extends BaseFragment {
    public static final String ADMIN_EMAIL = "nguyenvanquan7826@gmail.com";

    public static SupportFragment newInstance() {
        SupportFragment fragment = new SupportFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.support));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.support_fragment, container, false);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView tvVersion = view.findViewById(R.id.tvVersion);
        tvVersion.setText(getString(R.string.app_name) + " - ver: " + BuildConfig.VERSION_NAME);

        view.findViewById(R.id.tvShareThisApp).setOnClickListener(v ->
                SupportUtil.shareApp(getContext(), getString(R.string.choose_one_app_to_share), BuildConfig.APPLICATION_ID));
        view.findViewById(R.id.tvRate5Star).setOnClickListener(v ->
                SupportUtil.openPlayStore(getContext(), BuildConfig.APPLICATION_ID));
        view.findViewById(R.id.tvFeedback).setOnClickListener(v ->
                SupportUtil.sendEmail(getContext(), getString(R.string.feedback),
                        new String[]{ADMIN_EMAIL}, getString(R.string.feedback), "", ""));
    }
}
