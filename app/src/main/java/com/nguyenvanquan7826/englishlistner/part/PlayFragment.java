package com.nguyenvanquan7826.englishlistner.part;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nguyenvanquan7826.englishlistner.R;
import com.nguyenvanquan7826.englishlistner.base.BaseFragment;
import com.nguyenvanquan7826.englishlistner.util.DbLocalUtil;
import com.nguyenvanquan7826.englishlistner.util.TimeUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class PlayFragment extends BaseFragment {
    private static final String TAG = PlayFragment.class.getSimpleName();

    private static final String KEY_URI = "uri";
    private static final String KEY_FILE_NAME = "fileName";

    public static PlayFragment newInstance(String uri, String fileName) {
        PlayFragment fragment = new PlayFragment();
        Bundle args = new Bundle();
        args.putString(KEY_URI, uri);
        args.putString(KEY_FILE_NAME, fileName);
        fragment.setArguments(args);
        return fragment;
    }

    private String fileName = "";

    private MediaPlayer mediaPlayer;
    private SeekBar seekBar;
    private List<PartSong> partSongList = new ArrayList<>();
    private PartSongAdapter partSongAdapter;

    private int total = 0;
    private int indexSpeedSelect = 2;
    private TextView tvCurrent, tvSpeed;
    private ImageView ivPlay;

    private int[] idIvForward = {R.drawable.ic_forward_5, R.drawable.ic_forward_10, R.drawable.ic_forward_30,};
    private int[] idIvReplay = {R.drawable.ic_replay_5, R.drawable.ic_replay_10, R.drawable.ic_replay_30,};

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fileName = getArguments().getString(KEY_FILE_NAME);
        String uriString = getArguments().getString(KEY_URI);
        mediaPlayer = MediaPlayer.create(getContext(), Uri.parse(uriString));
        total = mediaPlayer.getDuration();
        mediaPlayer.setOnSeekCompleteListener(mp -> {
            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                mediaPlayer.start();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(fileName);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.play_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView rv = view.findViewById(R.id.rv);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        partSongAdapter = new PartSongAdapter(getContext(), partSongList).setOnItemClickListener(this::eventClickItem);
        rv.setAdapter(partSongAdapter);

        ivPlay = view.findViewById(R.id.ivPlay);
        ivPlay.setOnClickListener(v -> play(false));
        view.findViewById(R.id.ivCut).setOnClickListener(v -> split());
        tvSpeed = view.findViewById(R.id.ivSpeed);
        tvSpeed.setOnClickListener(v -> showPopupSpeed());

        int indexSecondReplay = DbLocalUtil.getSettingIndexSecondReplay(getContext());

        ImageView ivForward = view.findViewById(R.id.ivForward);
        ivForward.setImageResource(idIvForward[indexSecondReplay]);
        ivForward.setOnClickListener(v -> replayOrForward(DbLocalUtil.SECOND_REPLAY_ARR[indexSecondReplay]));

        ImageView ivReplay = view.findViewById(R.id.ivReplay);
        ivReplay.setImageResource(idIvReplay[indexSecondReplay]);
        ivReplay.setOnClickListener(v -> replayOrForward(-DbLocalUtil.SECOND_REPLAY_ARR[indexSecondReplay]));


        ((TextView) view.findViewById(R.id.tvTotal)).setText(TimeUtil.getTextOfTime(total));
        tvCurrent = view.findViewById(R.id.tvCurrent);

        seekBar = view.findViewById(R.id.seekbar);
        setupSeekBar();

        play(false);
        loadPartSong();
    }

    private void loadPartSong() {
        partSongList.clear();
        partSongList.addAll(DbLocalUtil.getPartSongList(getContext(), fileName));
        partSongAdapter.notifyDataSetChanged();
    }

    private void eventClickItem(int position, String tag) {
        if (TextUtils.isEmpty(tag)) {
            playItem(position);
        } else if (tag.equalsIgnoreCase(PartSongAdapter.TAG_DELETE)) {
            partSongAdapter.notifyItemRemoved(position);
            DbLocalUtil.removeSavePartSong(getContext(), fileName, partSongList.get(position).getId());
            partSongList.remove(position);
        } else if (tag.equalsIgnoreCase(PartSongAdapter.TAG_EDIT)) {
            PartSong partSong = partSongList.get(position);
            EditText editText = new EditText(getContext());
            editText.setHint(R.string.part_name);
            editText.setText(partSong.getName());
            editText.setSelection(partSong.getName().length());

            new AlertDialog.Builder(getContext())
                    .setTitle(R.string.edit_part)
                    .setView(editText)
                    .setPositiveButton(R.string.ok, (dialog, which) -> {
                        String name = editText.getText().toString().trim();
                        if (TextUtils.isEmpty(name)) {
                            Toast.makeText(getContext(), R.string.name_empty, Toast.LENGTH_SHORT).show();
                        } else {
                            partSong.setName(name);
                            partSongList.set(position, partSong);
                            partSongAdapter.notifyItemChanged(position);
                            DbLocalUtil.savePartSongList(getContext(), fileName, partSongList);
                        }
                    })
                    .setNegativeButton(R.string.cancel, null)
                    .show();
        }
    }

    private void play(boolean mustPause) {
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying() || mustPause) {
                mediaPlayer.pause();
                ivPlay.setImageResource(R.drawable.ic_play);
            } else {
                mediaPlayer.start();
                ivPlay.setImageResource(R.drawable.ic_pause);
            }
        }
    }

    private void split() {
        int time = mediaPlayer.getCurrentPosition();
        PartSong partSong = new PartSong(Calendar.getInstance().getTimeInMillis(),
                String.format(getString(R.string.part_n), (partSongList.size() + 1)), time);
        partSongList.add(partSong);
        partSongAdapter.notifyDataSetChanged();
        DbLocalUtil.addSavePartSong(getContext(), fileName, partSong);
    }

    private void showPopupSpeed() {
        if (Build.VERSION.SDK_INT >= 23) {
            double[] speeds = {0.5, 0.75, 1.0, 1.25, 1.5};
            String[] speedsText = new String[speeds.length];
            for (int i = 0; i < speedsText.length; i++) {
                speedsText[i] = "x" + speeds[i];
            }
            new AlertDialog.Builder(getContext())
                    .setTitle(R.string.change_speed)
                    .setSingleChoiceItems(speedsText, indexSpeedSelect, (dialogInterface, i) -> {
                        indexSpeedSelect = i;
                        tvSpeed.setText(String.valueOf(speeds[i]));
                        mediaPlayer.setPlaybackParams(mediaPlayer.getPlaybackParams().setSpeed((float) speeds[i]));
                        dialogInterface.dismiss();
                    })
                    .show();
        } else {
            Toast.makeText(getContext(), R.string.not_support, Toast.LENGTH_SHORT).show();
        }
    }

    private void playItem(int pos) {
        seekTo(partSongList.get(pos).getTime());
    }

    private void seekTo(int milisec) {
//        mediaPlayer.pause();
        mediaPlayer.seekTo(milisec);
        updateSeekBarAndTime();
    }

    private void replayOrForward(int sec) {
        int time = mediaPlayer.getCurrentPosition() / 1000;
        time += sec;
        time *= 1000;
        if (time < 0) time = 0;
        if (time > mediaPlayer.getDuration()) time = total;
        seekTo(time);
    }

    private void updateSeekBarAndTime() {
        int time = mediaPlayer.getCurrentPosition();
        int progress = time * 100 / total;
        seekBar.setProgress(progress);
        tvCurrent.setText(TimeUtil.getTextOfTime(time));
    }

    private void setupSeekBar() {
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    int time = progress * total / 100;
                    tvCurrent.setText(TimeUtil.getTextOfTime(time));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int index = total * seekBar.getProgress() / 100;
                seekTo(index);
            }
        });

        /*
          update seekbar each 1s
          */
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mediaPlayer != null) {
                    handler.postDelayed(this, 1000);

                    if (mediaPlayer.isPlaying()) {
                        updateSeekBarAndTime();
                    }
                }
            }
        }, 1000);
    }

//    @Override
//    public void onStop() {
//        super.onStop();
//        Log.e(TAG, "onStop");
//        play(true);
//    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }
}
