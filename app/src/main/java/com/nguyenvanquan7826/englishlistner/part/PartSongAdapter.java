package com.nguyenvanquan7826.englishlistner.part;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nguyenvanquan7826.englishlistner.R;
import com.nguyenvanquan7826.englishlistner.base.BaseRVAdapter;
import com.nguyenvanquan7826.englishlistner.util.TimeUtil;

import java.util.List;

public class PartSongAdapter extends BaseRVAdapter<PartSongAdapter.ViewHolder> {
    public static final String TAG_DELETE = "delete";
    public static final String TAG_EDIT = "edit";

    private Context context;
    private List<PartSong> partSongList;

    public PartSongAdapter(Context context, List<PartSong> documentFileList) {
        this.partSongList = documentFileList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_part_song, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        PartSong partSong = partSongList.get(i);
        viewHolder.tvName.setText(partSong.getName());
        viewHolder.tvTime.setText(TimeUtil.getTextOfTime(partSong.getTime()));
    }

    @Override
    public int getItemCount() {
        return partSongList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvTime;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvTime = itemView.findViewById(R.id.tvTime);

            itemView.findViewById(R.id.ivMore).setOnClickListener(v -> showPopupMenu(v, getAdapterPosition()));
            itemView.setOnClickListener(v -> clickItem(getAdapterPosition(), null));
        }
    }

    private void showPopupMenu(View v, int pos) {
        PopupMenu popupMenu = new PopupMenu(context, v);
        popupMenu.getMenuInflater().inflate(R.menu.item_part_menu, popupMenu.getMenu());
        popupMenu.show();

        popupMenu.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.menu_delete) {
                clickItem(pos, TAG_DELETE);
            }
            if (item.getItemId() == R.id.menu_edit) {
                clickItem(pos, TAG_EDIT);
            }
            return false;
        });
    }
}
