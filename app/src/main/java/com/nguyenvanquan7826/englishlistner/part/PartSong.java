package com.nguyenvanquan7826.englishlistner.part;

public class PartSong {
    private long id;
    private String name;
    private int time;

    public PartSong(long id, String name, int time) {
        this.id = id;
        this.name = name;
        this.time = time;
    }

    public PartSong() {
    }

    public long getId() {
        return id;
    }

    public PartSong setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public PartSong setName(String name) {
        this.name = name;
        return this;
    }

    public int getTime() {
        return time;
    }

    public PartSong setTime(int time) {
        this.time = time;
        return this;
    }
}
