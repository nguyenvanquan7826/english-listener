package com.nguyenvanquan7826.englishlistner.base;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.nguyenvanquan7826.englishlistner.R;

public class BaseActivity extends AppCompatActivity {

    public void showFragment(Fragment fragment, boolean isAddStack) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (isAddStack) ft.addToBackStack(null);
        ft.replace(R.id.layoutFragment, fragment).commit();
    }
}
