package com.nguyenvanquan7826.englishlistner.base;

import android.support.v7.widget.RecyclerView;

public abstract class BaseRVAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {
    private OnItemRVClick onItemClickListener;

    public <T extends BaseRVAdapter> T setOnItemClickListener(OnItemRVClick onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
        return (T) this;
    }

    protected void clickItem(int position, String tag) {
        if (onItemClickListener != null) onItemClickListener.onItemClick(position, tag);
    }

    public interface OnItemRVClick {
        void onItemClick(int position, String tag);
    }
}