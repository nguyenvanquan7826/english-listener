package com.nguyenvanquan7826.englishlistner.base;

import android.support.v4.app.Fragment;

public class BaseFragment extends Fragment {
    protected void setTitle(String title) {
        if (getActivity() != null) {
            getActivity().setTitle(title);
        }
    }
}
