package com.nguyenvanquan7826.englishlistner.settings;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nguyenvanquan7826.englishlistner.R;
import com.nguyenvanquan7826.englishlistner.base.BaseFragment;
import com.nguyenvanquan7826.englishlistner.util.DbLocalUtil;

public class SettingsFragment extends BaseFragment {
    public static final String ADMIN_EMAIL = "nguyenvanquan7826@gmail.com";

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private TextView tvSubTitleSettingSecondReplay;

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.settings));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.setting_fragment, container, false);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvSubTitleSettingSecondReplay = view.findViewById(R.id.tvSubTitleSecondReplay);
        int indexSelect = DbLocalUtil.getSettingIndexSecondReplay(getContext());
        String text = DbLocalUtil.SECOND_REPLAY_ARR[indexSelect] + "s";
        tvSubTitleSettingSecondReplay.setText(text);

        view.findViewById(R.id.layoutSettingSeoundReplay).setOnClickListener(v -> showDialogSettingSecondReplay());
    }

    private void showDialogSettingSecondReplay() {
        int indexSelect = DbLocalUtil.getSettingIndexSecondReplay(getContext());
        String listItems[] = new String[DbLocalUtil.SECOND_REPLAY_ARR.length];
        for (int i = 0; i < listItems.length; i++) {
            listItems[i] = DbLocalUtil.SECOND_REPLAY_ARR[i] + "s";
        }

        new AlertDialog.Builder(getContext())
                .setTitle(R.string.second_replay_and_forward)
                .setSingleChoiceItems(listItems, indexSelect, (dialogInterface, i) -> {
                    tvSubTitleSettingSecondReplay.setText(listItems[i]);
                    DbLocalUtil.saveSettingIndexSecondReplay(getContext(), i);
                    dialogInterface.dismiss();
                })
                .show();
    }
}
